export default function User({ user, createUser, disabled, className }) {
  const addUser = () => {
    createUser(user.id);
  };

  return(
    <div className="btn">
      <button className={className} disabled={disabled} onClick={addUser}>{user.name}</button>
    </div>
  );
}
