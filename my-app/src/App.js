import { useEffect, useState } from "react";
import User from "./User";
import NewUser from "./NewUser";
import "./styles.css";

const url = "https://jsonplaceholder.typicode.com/users";

export default function App() {
  const [users, setUsers] = useState([]);
  const [deleted, setDeleted] = useState(0);
  const [created, setCreated] = useState(0);
  const [addUsers, setAddUsers] = useState([]);

  useEffect(() => {
    fetchUsers();
  }, []);

  /*const fetchUsers = async () => {
    const res = await fetch(url);
    const data = await res.json();
    setAddUsers(data);
  };*/

  const fetchUsers = () => fetch(url)
  .then(res => res.json())
  .then(
    (result) => {
      setAddUsers(result);
    },
    (error) => {
      alert("Ошибка маршрута!")
    }
  )

  const deleteUser = (id) => {
    setUsers(users.filter((user) => user.id !== id));
    setDeleted(deleted + 1);
  };

  const createUser = (id) => {
    const user = addUsers.find((user) => user.id === id);
    setUsers([...users, user]);
    setCreated(created + 1);
  };

  return (
    <div className="App">
      <div className="user-list">
        {users.length > 0 ? (
          users.map((item) => (
            <User key={item.id} user={item} deleteUser={deleteUser} />
          ))
        ) : (
          <p className="user-list-empty">У вас нет пользователей в списке</p>
        )}
      </div>
      <div className="informer">
        <p className="informer-item">Удалено: {deleted}</p>
        <p className="informer-item">Добавлено: {created}</p>
      </div>
      <div className="btn-list">
        {addUsers.length > 0 ? (
          addUsers.map((item) => (users.length > 0 && users.find((user) => user.id === item.id)) ?
          (<NewUser key={item.id} user={item} disabled={'disabled'} createUser={createUser} />) : (<NewUser className={"action-button"} key={item.id} user={item} createUser={createUser} />)
        )) : (
          <p>нет записей</p>
        )}
      </div>
    </div>
  );
}
