export default function User({ user, deleteUser }) {
  const removeUser = () => {
    deleteUser(user.id);
  };

  return (
    <div className="user-item">
      <h3>{user.name ? user.name : ''}</h3>
      <p><span>phone:</span> {user.phone}</p>
      <p><span>website:</span> {user.website}</p>
      <p><span>email:</span> {user.email}</p>
      <p><span>company:</span> {user.company.name}</p>
      <div className="user-item-button">
        <button onClick={removeUser}>Удалить из списка</button>
      </div>
    </div>
  );
}
